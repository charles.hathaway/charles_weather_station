# Weather station

A basic application to record measurements.

## How to use it

Run the server in one terminal:

```
cargo run --release -- -l 0.0.0.0:8080 -u 0.0.0.0:8080 -d sqlite://database.db
```

Then create your first station:

```
curl \
    -H 'Content-Type: application/json' \
    -d '{"label":"home"}' \
    http://localhost:8080/stations
```

Note the response, which should look like:

```
{"id":"386c0c58-4530-486c-88f9-c2a442d162a8","label":"home"}
```

Using the ID, create a sensor attached to that station:

```
curl \
    -H 'Content-Type: application/json' \
    -d '{"sensor_type": "temp",
        "station": {
            "id":"e4fa707c-bbe6-475a-81de-7d396af696e1", "label":"home"
            }
        }' \
    http://localhost:8080/sensors
```

This should result in output like:

```
{"id":"35732bac-4e85-445a-9b7a-204311d35eef","station":{"id":"e4fa707c-bbe6-475a-81de-7d396af696e1","label":"home"},"sensor_type":"temp","measurements":null
```

### Sending a measurement

To send measurements, you need to send a UDP packet with the format:

```
sensor_type=1234.54,id=STATION_ID#CHECKSUM
```

In the above example, the CHECKSUM is a CRC32 of 
`sensor_type=1234.54,id=STATION_ID#STATION_KEY`. This
means you need the station key to send measurements. It
is not possible to get that key through the web UI, but
you can with some SQL:

```
sqlite3 database.db 'select label,id,key from stations'
```

Using this, you can send a measurement with the built-in measurement tool

```
cargo run --release --bin measurement -- 127.0.0.1:8080 $STATION_ID $STATION_KEY sensor_type=1234.54
```