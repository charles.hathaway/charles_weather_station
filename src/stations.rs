use actix_web::{web, HttpResponse};
use serde::{Serialize, Deserialize};

use super::{
    dal::{
        self,
        Database,
    },
    errors::ApplicationError,
};

#[derive(Debug, Serialize, Deserialize)]
pub struct Station<'a> {
    pub id: Option<&'a str>,
    pub label: &'a str,
}

impl<'a> From<&'a dal::Station> for Station<'a> {
    fn from(s: &'a dal::Station) -> Self {
        Station{
            id: Some(&s.id),
            label: &s.label,
        }
    }
}

async fn find_all<D>(db: web::Data<D>) -> Result<HttpResponse, ApplicationError>
where D: Database {
    let stations = db.find_all_stations()?;
    let stations: Vec<Station> = stations
        .iter()
        .map(|x| Station::from(x)).collect();
    Ok(HttpResponse::Ok().json(stations))
}

async fn create<D>(db: web::Data<D>, req: web::Bytes) -> Result<HttpResponse, ApplicationError>
where D: Database {
    // parse the request; we do it this way to avoid lifetime issues
    let mut val: Station = serde_json::from_slice(&req)?;
    let id = db.insert_station(val.label)?;
    val.id = Some(&id);
    Ok(HttpResponse::Ok().json(val))
}

async fn find<D>(db: web::Data<D>, id: String) -> Result<HttpResponse, ApplicationError>
where D: Database {
    let station = db.station_by_id(&id)?;
    Ok(HttpResponse::Ok().json(Station::from(&station)))
}

pub fn init_routes<D: Database + 'static>(config: &mut web::ServiceConfig) {
    config.route("/stations", web::get().to(|db: web::Data<D>| find_all(db)));
    config.route("/stations", web::post().to(|db: web::Data<D>, req| create(db, req)));
    config.route("/stations/{id}", web::get().to(|db: web::Data<D>, id: String| find(db, id)));
 }
