use std::{
    net::UdpSocket,
    thread,
};

use actix_web::{
    App,
    HttpServer,
    middleware::Logger,
};

use clap::Clap;
use log::{error};

mod dal;
mod stations;
mod sensors;
mod errors;
mod udp;

use dal::{
    Database,
    sqlite::SqliteDatabase,
};

use udp::{
    UdpServer,
    udp_server,
};

/// Bring up a weather station server.
#[derive(Clap)]
#[clap(version = "1.0", author = "Someone")]
struct Opts {
    /// Address to listen for HTTP connections on
    #[clap(short, long, default_value = "127.0.0.1:8080")]
    listen: String,
    /// Address to listen for UDP messages on (measurements)
    #[clap(short, long, default_value = "127.0.0.1:8080")]
    udp: String,
    /// Database to use; currently, only sqlite is supported
    #[clap(short, long, default_value = "sqlite://database.db")]
    database: String,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let opts: Opts = Opts::parse();
    env_logger::init();
    if !opts.database.starts_with("sqlite://") {
        error!("Only sqlite is supported as a database");
        return Ok(());
    }
    let db = &opts.database["sqlite://".len()..];
    let db = dal::sqlite::SqliteDatabase::new(db).unwrap();
    db.migrate().unwrap();

    // Start the UDP server
    let udp_server_socket = UdpSocket::bind(opts.udp)?;
    let db2 = db.clone();
    thread::spawn(move || {
        udp_server(UdpServer::new(udp_server_socket), db2);
    });
    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(Logger::new("%a %{User-Agent}i"))
            .data(db.clone())
            .configure(stations::init_routes::<SqliteDatabase>)
            .configure(sensors::init_routes::<SqliteDatabase>)
    })
    .bind(opts.listen)?
    .run()
    .await
}
