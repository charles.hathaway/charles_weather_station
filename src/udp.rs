use std::{
    net::UdpSocket,
    collections::HashMap,
};

use log::{trace, warn, info};

use nom;
use nom::sequence::separated_pair;
use nom::bytes::complete::tag;
use nom::number::complete::double;
use nom::bytes::complete::take_till;
use nom::combinator::opt;
use crc::{crc32, Hasher32};

use super::{
    dal::{
        Database,
        Sensor,
    },
    errors::UdpError,
};

pub struct Message {
    pub measures: HashMap<String, f64>,
    pub station_id: String,
    // the digest so far; notably missing the station key
    pub digest: crc32::Digest,
    // the expected checksum
    pub checksum: usize,
}

impl std::fmt::Debug for Message {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Message")
         .field("measures", &self.measures)
         .field("station_id", &self.station_id)
         .field("checksum", &self.checksum)
         .finish()
    }
}

/// runs an infinite loop to process UDP messages
pub fn udp_server<D: Database>(server: UdpServer, db: D) {
    // run the UDP server
    for msg in server {
        info!("UDP message {:?}", msg);
        let station = match db.station_by_id(&msg.station_id) {
            Ok(s) => s,
            Err(e) => {
                info!("Could not find specified station {:?}", e);
                continue
            }
        };
        // validate the message CRC
        let mut digest = msg.digest;
        if let Some(key) = &station.key {
            digest.write(key.as_bytes());
        } else {
            info!("No key for station {}; ignoring message", station.id);
            continue
        }
        let got_checksum = digest.sum32() as usize;
        if got_checksum != msg.checksum {
            info!("Dropping message with invalid checksum (got {}, want {})", got_checksum, msg.checksum);
        }
        let sensors = match db.sensors_by_station(&station) {
            Ok(s) => s,
            Err(e) => {
                info!("Could not find sensors for station {}: {:?}", msg.station_id, e);
                continue
            }
        };
        // build label => id mapping
        let mut sensor_map: HashMap<String, &Sensor> = HashMap::default();
        for s in &sensors {
            sensor_map.insert(s.label.clone(), s);
        }
        for (sensor, value) in msg.measures {
            let sensor_id = match sensor_map.get(&sensor) {
                Some(s) => &s.id,
                None => {
                    info!("Failed to find sensor with label {}", sensor);
                    continue
                }
            };
            if let Err(e) = db.blind_insert_measurement(&sensor_id, value) {
                warn!("error writing measurement to database: {:?}", e);
            }
        }
    }
}


/// Parses a slice with the form "key=value", seperated by commas. Expects only
/// valid fields; pressure, air_temp, voltage, signal, id.
fn parse_message(mut input: &[u8]) -> Result<(&[u8], Message), UdpError> {
    let mut values: HashMap<String, f64> = HashMap::new();
    // a reference to the start of input
    let full_input = input;
    let full_length = input.len();
    let mut station_id = String::new();
    while input.len() != 0 {
        // Check to see if we hit the 'end of message' tag
        let (rest, val) = opt(tag("#"))(input)?;
        if let Some(_) = val {
            input = rest;
            break;
        }
        trace!("parsing next measure; remaining: {}", String::from_utf8_lossy(input));
        let (rest, (name, value)) = separated_pair(
            take_till(|b| b == b'=' ),
            tag("="),
            take_till(|b| b == b',' || b == b'#')
        )(input)?;
        let (rest, _) = opt(tag(","))(rest)?;
        input = rest;
        if name == b"id" {
            station_id = String::from_utf8_lossy(&value).to_string();
        } else {
            let (_, value) = double(value)?;
            values.insert(String::from_utf8_lossy(name).to_string(), value);
        }
    }
    // remaining message is crc sum; calculate based on what we've so far
    let message_length = full_length - input.len();
    let digest = perform_crc_check(&full_input[..message_length]);
    let (rest, checksum) = double(input)?;
    let checksum = checksum as usize;
    Ok((rest, Message{
        measures: values,
        checksum,
        digest,
        station_id,
    }))
}

fn perform_crc_check(input: &[u8]) -> crc32::Digest {
    let mut digest = crc32::Digest::new(crc32::IEEE);
    digest.write(input);
    digest
}

pub struct UdpServer {
    buff: Vec<u8>,
    sock: UdpSocket,
}

impl UdpServer {
    /// constructs a new server which will run on the given address
    pub fn new(sock: UdpSocket) -> Self {
        UdpServer {
            // max udp packet length
            buff: vec![0; 65535],
            sock,
        }
    }
}

impl Iterator for UdpServer {
    type Item = Message;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let read_result = self.sock.recv(&mut self.buff);
            trace!("read message {:?}", read_result);
            if read_result.is_err() {
                warn!("error reading data from socket; {:?}", read_result);
                return None;
            }
            let count = read_result.unwrap();
            let slice = &self.buff[0..count];
            // parse the message
            let msg = parse_message(slice);
            if msg.is_err() {
                warn!("dropping invalid message; error was {:?}", msg);
                continue;
            }
            let msg = msg.unwrap();
            let msg = msg.1;
            return Some(msg);
        }
    }
}

#[cfg(test)]
mod tests {
    use std::thread;
    use super::*;

    const VALID_MESSAGE: &[u8] = b"pressure=987.89,air_temp=25.46,voltage=3.93,signal=-42,id=hello-world#12345";

    fn create_udp_server() -> Result<(UdpSocket, UdpServer), Box<dyn std::error::Error>> {
        let server_socket = UdpSocket::bind(":::0")?;
        let client_socket = UdpSocket::bind(":::0")?;
        client_socket.connect(server_socket.local_addr()?)?;
        Ok((client_socket, UdpServer::new(server_socket)))
    }

    #[test]
    fn test_iterate_some_messages() {
        let (client, server) = create_udp_server().unwrap();
        thread::spawn(move || {
            client.send(VALID_MESSAGE).unwrap();
        });
        let mut count = 0;
        for _ in server {
            count += 1;
            break;
        }
        assert_eq!(1, count);
    }

    #[test]
    fn test_parse_message() {
        let (rest, _) = parse_message(VALID_MESSAGE).unwrap();
        assert_eq!(rest.len(), 0);
        // try a weirdly formed message
        let res = parse_message(b"hello my name is bob");
        assert_eq!(true, res.is_err());
    }
}
