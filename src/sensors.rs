use actix_web::{
    web,
    HttpResponse,
    HttpRequest,
};
use serde::{Serialize, Deserialize};

use super::{
    dal::{
        self,
        Database,
    },
    errors::ApplicationError,
    stations::Station,
};

#[derive(Debug, Serialize, Deserialize)]
struct Sensor<'a> {
    pub id: Option<&'a str>,
    pub station: Option<Station<'a>>,
    pub sensor_type: &'a str,
    pub measurements: Option<Vec<Measurement>>,
}

impl<'a> From<&'a dal::Sensor<'a>> for Sensor<'a> {
    fn from(s: &'a dal::Sensor) -> Self {
        Sensor{
            id: Some(&s.id),
            station: Some(s.station.into()),
            sensor_type: &s.sensor_type,
            measurements: None,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Measurement {
    time: String,
    value: f64,
}

impl<'a> From<&'a dal::Measurement<'a>> for Measurement {
    fn from(m: &'a dal::Measurement<'a>) -> Self {
        Measurement {
            time: m.time.to_rfc3339(),
            value: m.value,
        }
    }
}

async fn find_all<D>(db: web::Data<D>) -> Result<HttpResponse, ApplicationError>
where D: Database {
    let stations = db.find_all_stations()?;
    let sensors = db.find_all_sensors(&stations)?;
    let sensors: Vec<Sensor> = sensors
        .iter()
        .map(|x| Sensor::from(x)).collect();
    Ok(HttpResponse::Ok().json(sensors))
}

async fn create<D>(db: web::Data<D>, req: web::Bytes) -> Result<HttpResponse, ApplicationError>
where D: Database {
    // parse the request; we do it this way to avoid lifetime issues
    let mut val: Sensor = serde_json::from_slice(&req)?;
    let station = match &val.station {
        Some(s) => s,
        None => return Ok(HttpResponse::BadRequest().body("No station provided")),
    };
    let station_id = match station.id {
        Some(s) => s,
        None => return Ok(HttpResponse::BadRequest().body("No station id provided")),
    };
    let id = db.insert_sensor(station_id, val.sensor_type)?;
    val.id = Some(&id);
    Ok(HttpResponse::Ok().json(val))
}

async fn find<D>(db: web::Data<D>, req: HttpRequest) -> Result<HttpResponse, ApplicationError>
where D: Database {
    let mut empty_station = dal::Station{
        id: "".into(),
        label: "".into(),
        key: Some("".into()),
    };
    let id: &str = req.match_info().get("id").unwrap_or("");
    let sensor = db.sensor_by_id(&id, &mut empty_station)?;
    let measurements = db.measurements_by_sensor(&sensor)?;
    let mut sensor = Sensor::from(&sensor);
    sensor.measurements = Some(measurements.iter().map(|m| m.into()).collect());
    Ok(HttpResponse::Ok().json(sensor))
}

pub fn init_routes<D: Database + 'static>(config: &mut web::ServiceConfig) {
    config.route("/sensors", web::get().to(|db: web::Data<D>| find_all(db)));
    config.route("/sensors", web::post().to(|db: web::Data<D>, req| create(db, req)));
    config.route("/sensors/{id}", web::get().to(|db: web::Data<D>, req: HttpRequest| find(db, req)));
 }
