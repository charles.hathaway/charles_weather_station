use std::{
    sync::{
        MutexGuard,
        PoisonError,
    },
};

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::Connection;

pub mod sqlite;

#[derive(Debug)]
pub enum DatabaseError {
    Generic(String),
}

impl From<rusqlite::Error> for DatabaseError {
    fn from(e: rusqlite::Error) -> Self {
        DatabaseError::Generic(format!("resqlite: {}", e))
    }
}

impl From<PoisonError<MutexGuard<'_, Connection>>> for DatabaseError {
    fn from(e: PoisonError<MutexGuard<'_, Connection>>) -> Self {
        DatabaseError::Generic(format!("mutex error: {}", e))
    }
}

impl From<chrono::ParseError> for DatabaseError {
    fn from(e: chrono::ParseError) -> Self {
        DatabaseError::Generic(format!("timestamp: {}", e))
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Station {
    pub id: String,
    pub label: String,
    // this may be empty if the station was not loaded from the database
    pub key: Option<String>,
}

#[derive(Debug, Eq, PartialEq)]
pub struct Sensor<'a> {
    pub id: String,
    pub label: String,
    pub station: &'a Station,
    pub sensor_type: String,
}

pub(super) fn uuid() -> String {
    let my_uuid = Uuid::new_v4();
    format!("{}", my_uuid.to_hyphenated())
}

pub struct Measurement<'a> {
    pub sensor: &'a Sensor<'a>,
    pub time: DateTime<Utc>,
    // TODO: should we be using a fixed-point number here?
    pub value: f64,
}

pub trait Database: Clone + Sync + Send {
    fn migrate(&self) -> Result<(), DatabaseError>;
    // inserts a station with the given label, and returns it's ID
    fn insert_station(&self, label: &str) -> Result<String, DatabaseError>;
    fn station_by_id(&self, id: &str) -> Result<Station, DatabaseError>;
    fn find_all_stations(&self) -> Result<Vec<Station>, DatabaseError>;

    fn insert_sensor(&self, station_id: &str, sensor_type: &str) -> Result<String, DatabaseError>;
    // Finds all sensors and associates them with the provided stations
    fn find_all_sensors<'a>(&self, stations: &'a [Station]) -> Result<Vec<Sensor<'a>>, DatabaseError>;
    fn sensors_by_station<'a>(&self, station: &'a Station) -> Result<Vec<Sensor<'a>>, DatabaseError>;
    fn sensor_by_id<'a>(&self, id: &str, station: &'a mut Station) -> Result<Sensor<'a>, DatabaseError>;

    // blindly inserts a measurement without validating sensor_id
    fn blind_insert_measurement(&self, sensor_id: &str, value: f64) -> Result<(), DatabaseError>;
    fn measurements_by_sensor<'a>(&self, sensor: &'a Sensor) -> Result<Vec<Measurement<'a>>, DatabaseError>;
}