use std::{
    sync::{
        Arc,
        Mutex,
    },
    collections::HashMap,
};
use log::{info};

use chrono::prelude::*;

use rusqlite::{
    Connection,
    params,
};

use super::{
    Database,
    DatabaseError,
    Station,
    Sensor,
    Measurement,
    uuid,
};

#[derive(Clone)]
pub struct SqliteDatabase {
    conn: Arc<Mutex<Connection>>,
}

impl SqliteDatabase {
    pub fn new(path: &str) -> Result<Self, DatabaseError> {
        Ok(SqliteDatabase {
            conn: Arc::new(Mutex::new(Connection::open(path)?)),
        })
    }
}

impl Database for SqliteDatabase {
    fn migrate(&self) -> Result<(), DatabaseError> {
        self.conn.lock()?.execute_batch("
            CREATE TABLE IF NOT EXISTS Stations (
                id TEXT PRIMARY KEY,
                label TEXT NOT NULL,
                key STRING NOT NULL
            );

            CREATE TABLE IF NOT EXISTS Sensors (
                id TEXT PRIMARY KEY,
                -- TODO: delete this field
                label TEXT DEFAULT '',
                station_id TEXT NOT NULL,
                sensor_type STRING NOT NULL
            );

            CREATE INDEX IF NOT EXISTS SensorByStation
            ON Sensors(station_id);

            CREATE TABLE IF NOT EXISTS Measurements (
                sensor_id TEXT NOT NULL,
                time DATETIME DEFAULT CURRENT_TIMESTAMP,
                value REAL
            );

            CREATE INDEX IF NOT EXISTS MeasurementBySensor
            ON Measurements(sensor_id, time);
        ")?;
        Ok(())
    }

    fn insert_station(&self, label: &str) -> Result<String, DatabaseError> {
        let id = uuid();
        let key = uuid();
        self.conn.lock()?.execute("
            INSERT INTO Stations (id, label, key) VALUES (?1, ?2, ?3)
        ", params![id, label, key])?;
        Ok(id)
    }

    fn station_by_id(&self, id: &str) -> Result<Station, DatabaseError> {
        Ok(self.conn.lock()?.query_row("
            SELECT s.id,
                s.label,
                s.key
            FROM Stations s
            WHERE s.id = ?
        ", vec!(id), |f| {
            Ok(Station{
                id: f.get(0)?,
                label: f.get(1)?,
                key: Some(f.get(2)?),
            })
        })?)
    }

    fn find_all_stations(&self) -> Result<Vec<Station>, DatabaseError> {
        let conn = self.conn.lock()?;
        let mut stmt = conn.prepare("
            SELECT t1.id,
                t1.label,
                t1.key
            FROM Stations t1
        ")?;
        let mut rows = stmt.query(params![])?;
        let mut stations = vec!();
        while let Some(row) = rows.next()? {
            stations.push(Station{
                id: row.get(0)?,
                label: row.get(1)?,
                key: Some(row.get(2)?),
            });
        }
        Ok(stations)
    }

    fn insert_sensor(&self, station_id: &str, sensor_type: &str) -> Result<String, DatabaseError> {
        // TODO: should we validate that the station exists?
        let id = uuid();
        self.conn.lock()?.execute("
            INSERT INTO Sensors (id, station_id, sensor_type) VALUES (?1, ?2, ?3)
        ", params![id, station_id, sensor_type])?;
        Ok(id)
    }

    fn find_all_sensors<'a>(&self, stations: &'a [Station]) -> Result<Vec<Sensor<'a>>, DatabaseError> {
        let conn = self.conn.lock()?;
        let mut stmt = conn.prepare("
            SELECT t1.id,
                t1.label,
                t2.id,
                t1.sensor_type
            FROM Sensors t1
            INNER JOIN Stations t2
            ON (t1.station_id = t2.id)
        ")?;
        let mut rows = stmt.query(params![])?;
        // build a mapping of station => station_id
        let mut stations_map: HashMap<String, &Station> = HashMap::default();
        for station in stations {
            stations_map.insert(station.id.clone(), station);
        }
        let mut sensors = vec!();
        while let Some(row) = rows.next()? {
            let station_id: String = row.get(2)?;
            let station = stations_map.get(&station_id);
            // if we don't have a matching station, skip the sensor
            if station.is_none() {
                continue;
            }
            sensors.push(Sensor{
                id: row.get(0)?,
                label: row.get(1)?,
                station: station.unwrap(),
                sensor_type: row.get(3)?,
            });
        }
        Ok(sensors)
    }

    fn sensors_by_station<'a>(&self, station: &'a Station) -> Result<Vec<Sensor<'a>>, DatabaseError> {
        let conn = self.conn.lock()?;
        let mut stmt = conn.prepare("
            SELECT t1.id,
                t1.label,
                t1.sensor_type
            FROM Sensors t1
            WHERE t1.station_id = ?
        ")?;
        let mut rows = stmt.query(params![station.id])?;
        let mut sensors = vec!();
        while let Some(row) = rows.next()? {
            sensors.push(Sensor{
                id: row.get(0)?,
                label: row.get(1)?,
                station: station,
                sensor_type: row.get(2)?,
            });
        }
        Ok(sensors)
    }

    fn sensor_by_id<'a>(&self, id: &str, empty_station: &'a mut Station) -> Result<Sensor<'a>, DatabaseError> {
        let conn = self.conn.lock()?;
        let mut stmt = conn.prepare("
            SELECT t1.id,
                t1.label,
                t1.sensor_type,
                t1.station_id,
                t2.label,
                t2.key
            FROM Sensors t1
            LEFT JOIN Stations t2
            ON (t1.station_id = t2.id)
            WHERE t1.id = ?
        ")?;
        info!("id: {}", id);
        let mut rows = stmt.query(params![id])?;
        if let Some(row) = rows.next()? {
            empty_station.id = row.get(3)?;
            empty_station.label = row.get(4)?;
            empty_station.key = row.get(5)?;
            return Ok(Sensor{
                id: row.get(0)?,
                label: row.get(1)?,
                sensor_type: row.get(2)?,
                station: empty_station,
            });
        } else {
            return Err(DatabaseError::Generic("Sensor not found".into()));
        }
    }

    fn blind_insert_measurement(&self, sensor_id: &str, value: f64) -> Result<(), DatabaseError> {
        self.conn.lock()?.execute("
            INSERT INTO Measurements (sensor_id, value) VALUES (?1, ?2)
        ", params![sensor_id, value])?;
        Ok(())
    }

    fn measurements_by_sensor<'a>(&self, sensor: &'a Sensor) -> Result<Vec<Measurement<'a>>, DatabaseError> {
        let conn = self.conn.lock()?;
        let mut stmt = conn.prepare("
            SELECT t1.time
                , t1.value
            FROM Measurements t1
            WHERE t1.sensor_id = ?
            ORDER BY t1.time DESC
            LIMIT 100
        ")?;
        let mut rows = stmt.query(params![sensor.id])?;
        let mut measurements = vec!();
        while let Some(row) = rows.next()? {
            measurements.push(Measurement{
                sensor,
                time: Utc.datetime_from_str(&row.get::<usize, String>(0)?, "%Y-%m-%d %H:%M:%S")?,
                value: row.get(1)?,
            });
        }
        Ok(measurements)
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;

    pub fn testdb() -> SqliteDatabase {
        let db = SqliteDatabase::new(":memory:").unwrap();
        db.migrate().unwrap();
        db
    }
    #[test]
    fn insert_and_retrieve_station() {
        let db = testdb();
        let station = db.insert_station("Hello!").unwrap();
        let got_station = db.station_by_id(&station).unwrap();
        assert_eq!(got_station.label, "Hello!");
        let got_stations = db.find_all_stations().unwrap();
        assert_eq!(got_stations, vec!(got_station));
    }

    #[test]
    fn insert_and_retrieve_sensors() {
        let db = testdb();
        let station = db.insert_station("Hello!").unwrap();
        let station = db.station_by_id(&station).unwrap();
        let mut sensors = vec!("a", "b");
        for sensor in &sensors {
            db.insert_sensor(&station.id, sensor).unwrap();
        }
        let mut got_sensors = db.sensors_by_station(&station).unwrap();
        sensors.sort_by(|a, b| a.cmp(&b));
        got_sensors.sort_by(|a, b| a.label.cmp(&b.label));
        assert_eq!(got_sensors.iter().map(|x| &x.sensor_type).collect::<Vec<&String>>(),
            sensors);
        let stations_vec = vec!(station.clone());
        let got_sensors = db.find_all_sensors(&stations_vec).unwrap();
        assert_eq!(got_sensors.iter().map(|x| &x.sensor_type).collect::<Vec<&String>>(),
            sensors);
    }

    #[test]
    fn insert_blind_measurements() {
        let db = testdb();
        let station = db.insert_station("Hello!").unwrap();
        let station = db.station_by_id(&station).unwrap();
        let sensor = Sensor{
            id: "123".into(),
            label: "some label".into(),
            station: &station,
            sensor_type: "some kind".into(),
        };
        let mut want_vals = vec!(1.0, 2.0, 3.0, 4.0, 5.0);
        for val in &want_vals {
            db.blind_insert_measurement(&sensor.id, *val).unwrap();
        }
        let got_measurements = db.measurements_by_sensor(&sensor).unwrap();
        let got_vals: Vec<f64> = got_measurements.iter().map(|v| v.value).collect();
        // The readings will be returned in reverse (newest first)
        want_vals.reverse();
        assert_eq!(got_vals, want_vals);
    }
}