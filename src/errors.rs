use std::fmt;
use actix_web::{
    HttpResponse,
    ResponseError,
    http::StatusCode,
};
use serde_json::json;
use log::{warn};

use super::dal::DatabaseError;

#[derive(Debug)]
pub struct ApplicationError {
    status_code: Option<StatusCode>,
    message: String,
}

impl fmt::Display for ApplicationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&format!("{:?}", self))
    }
}

impl ResponseError for ApplicationError {
    fn error_response(&self) -> HttpResponse {
        warn!("Database error: {:#?}", self);
        let status_code = self.status_code.unwrap_or(StatusCode::INTERNAL_SERVER_ERROR);
        // Consider just using if/else where; like so
        let error_message = if status_code.as_u16() < 500 {
            self.message.clone()
        } else {
            "Internal server error".to_string()
        };
        HttpResponse::build(status_code).json(json!({ "message": error_message }))
    }
}

impl From<DatabaseError> for ApplicationError {
    fn from(e: DatabaseError) -> Self {
        warn!("Database error: {:#?}", e);
        ApplicationError{
            status_code: Some(StatusCode::INTERNAL_SERVER_ERROR),
            message: "Problem accessing database".into(),
        }
    }
}

impl From<serde_json::Error> for ApplicationError {
    fn from(e: serde_json::Error) -> Self {
        ApplicationError {
            status_code: Some(StatusCode::BAD_REQUEST),
            message: format!("Problem parsing json: {}", e),
        }
    }
}

#[derive(Debug, Clone)]
pub enum UdpError {
    Custom(String),
}

impl<I> From<nom::Err<(I, nom::error::ErrorKind)>> for UdpError {
    fn from(_: nom::Err<(I, nom::error::ErrorKind)>) -> Self {
        UdpError::Custom("Problem parsing UDP message".into())
    }
}